<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 ie7" lang="en" > <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js modern" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<title>Annual 2014 Support Staff Conference @ FCOE</title>

  <!-- IE Fixes: HTML5 Tags/IE8 Polyfill -->
  <!-- <script type="text/javascript" src="<?= base_path() . $directory ?>/bower_components/aight/aight.js"></script> -->
  <!--[if lt IE 9]>

    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
    <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>

  <![endif]-->

  <!-- Included CSS Files -->
  <?php print $styles ?>
  <!-- <link href="<?= base_path() . $directory ?>/stylesheets/app.css" rel="stylesheet" /> -->

  <!-- Moderniz it -->
  <script src="<?= base_path() . $directory ?>/bower_components/modernizr/modernizr.js"></script>

</head>
<body class="<?= $classes ?>">

<?
  echo $page_top;
  echo $page;
  echo $scripts;
  echo $page_bottom;
?>

  <!-- <script src="http://drupal.ssc2014:35729/livereload.js"></script> -->


  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
  <!-- <script src="<?= base_path() . $directory ?>/bower_components/jquery/dist/jquery.js"></script> -->
  <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>

  <script src="<?= base_path() . $directory ?>/bower_components/foundation/js/foundation.js"></script>
  <script src="<?= base_path() . $directory ?>/bower_components/foundation/js/foundation/foundation.orbit.js"></script>
  <script src="<?= base_path() . $directory ?>/bower_components/foundation/js/foundation/foundation.reveal.js"></script>
  <script src="<?= base_path() . $directory ?>/js/app.js"></script>

	
  <!-- Application Javascript, safe to override -->

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-39962334-1', 'fcoe.org');
  ga('send', 'pageview');

  </script>



</body>
</html>
