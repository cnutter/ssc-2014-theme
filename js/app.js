$(document).foundation({

  orbit: {
		animation: 'fade',
		animationSpeed: 1200,
		advanceSpeed: 5500,
		timer_speed: 10000, // Sets the amount of time in milliseconds before transitioning a slide
		pause_on_hover: true, // Pauses on the current slide while hovering
		resume_on_mouseout: true, // If pause on hover is set to true, this setting resumes playback after mousing out of slide
		next_on_click: true, // Advance to next slide on click
		animation_speed: 500, // Sets the amount of time in milliseconds the transition between slides will las
		slide_number: false,
		bullets: false,

		timer_container_class: 'orbit-fake-for-noshow', // Class name given to the timer
		timer_paused_class: 'orbit-fake-for-noshow-paused', // Class name given to the paused button
		timer_progress_class: 'orbit-fake-for-noshow-progress'

  },
  reveal: {
        animation: 'fadeAndPop',
        close_on_background_click: true,
        close_on_esc: true,
        dismiss_modal_class: 'close-reveal-modal',
        bg_class: 'reveal-modal-bg'
    }
}

);


//$('#webform-client-form-15 input[type="submit"]').attr('disabled','disabled');	
$('#webform-client-form-15 input[type="submit"]').addClass('button').addClass('radius');
$('#sidebar #edit-submit').addClass('button').addClass('radius');
$('#sidebar #edit-submit').css('padding', '7px 14px');
$('#side-button-block').css('padding', '9px 14px');

$('#scrolldiv').bind('scroll', function() {
	if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)
	{
		$('.close-reveal-modal').css('display','block');
		$('.no-close').css('display','none');
	}
});

$('.close-reveal-modal').click(function() {
	//$('#webform-client-form-15 input[type="submit"]').removeAttr('disabled');
	//$('#edit-submitted-nomination-agreement-of-terms-i-have-read-and-accept-the-terms-1').attr('checked','checked');	
});

// IE 8 Dropdown support
$(".has-dropdown").hover(
  function () { 
    $('ul', this).css("display", "block");
  },
  function () {
    $('ul', this).css("display", "none");
  } 
);