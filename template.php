<?php

function ssc2014_preprocess_image(&$variables) {
  unset(
    $variables['width'],
    $variables['height'],
    $variables['attributes']['width'],
    $variables['attributes']['height']
  );
}

function links_to($string, $url=false) {
  if ($url) {
    return '<a href="' . $url . '">' . $string . '</a>';
  } else {
    return $string;
  }
}


function ssc2014_preprocess_page(&$variables) {
  if (!empty($variables['node']) && !empty($variables['node']->type)) {
    $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
  }
}


?>