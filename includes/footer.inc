  <!-- @Footer.start -->

  <footer>
    <div class="row footing">
    <div class="flourish_footer columns show-for-medium-up"></div>
        <div class="large-1 columns">
          <ul class="primary">
            <li><a href="/">Home</a></li>
            <li><a href="/vendors">Vendors</a></li>
            <li><a href="/about/contact">Contact</a></li>
          </ul>  
        </div>
        <div class="large-3 columns">
          <ul class="secondary">
            <li><a href="/agenda">Agenda</a></li>
            <li><a href="/agenda/speakers">Keynote Speakers</a></li>
            <li><a href="/sessions">Breakout Sessions</a></li>
          </ul>  
        </div>
        <div class="large-6 columns end">
          <ul class="tertiary">
            <li><a href="/about">About</a></li>
            <li><a href="/about/apy-entry-form">A.P.Y.</a></li>
            <li><a href="/about/past-conferences">Past Conferences</a></li>
            <li><a href="/about/testimonials">Testimonials</a></li>
            <li><a href="/about/welcome-letter">Welcome Letter</a></li>
          </ul>
        </div>

      </div>

  </footer>
  <!-- @Footer.end -->


