<!-- FCOE -->
<div id="fcoe-banner">
  <div class="row">
    <div class="large-2 medium-12 columns large-offset-1">
      <div class="fcoe-logo"><a href="http://www.fcoe.org"></a></div>
    </div>
    <div class="large-3 medium-3 show-for-large-up columns">
      <h6 class="antialiased">Fresno County Office of Education </h6>
    </div>
    <div class="large-6 medium-6 show-for-large-up columns">
      <h6  class="antialiased">Jim A. Yovino, Superintendent of Schools</h6>
    </div>
  </div>
</div>
<!-- FCOE -->





  <!-- @Header.start -->
  <div class="row header">

    <div class="large-6 medium-6 columns">
      <h1><a href="/"><img src="<?= base_path() . $directory ?>/images/logo_ssc_header_v2.png" alt="Support Staff Conference"></a></h1>
    </div>

    <div class="large-6 medium-6 columns">

        <div class="deadline show-for-large-up">
          <p><a href="https://annualssc2014.eventbrite.com/"><span>Deadline to Register</span> April 24<sup>th</sup></a></p>
          <div class="triangle"></div>
        </div>

      <nav class="top-bar" data-topbar>

        <ul class="title-area">
          <li class="name show-for-small">
            <h1><a href="#">Navigation</a></h1>
          </li>
          <li class="toggle-topbar menu-icon show-for-small"><a href="#"></a></li>
        </ul>

        <section class="top-bar-section">

          <ul class="right">
            <li class="has-dropdown">
              <a href="/agenda">Agenda</a>
              <ul class="dropdown left">
                <li class="first leaf"><a href="/agenda/keynote-speaker">Keynote Speaker</a></li>              
                <li class="leaf"><a href="/agenda/speakers">Speakers</a></li>
                <li class="last leaf"><a href="/sessions">Breakout Sessions</a></li>
              </ul>
            </li>
            <li class="leaf"><a href="/vendors">Vendors</a></li>
            <li class="has-dropdown">
              <a href="/about">About</a>
              <ul class="dropdown left">
                <li class="first leaf"><a href="/about/apy-entry-form">A.P.Y. Entry Form</a></li>
                <li class="leaf"><a href="/about/contact">Contact</a></li>
                <li class="leaf"><a href="/about/past-conferences">Past Conferences</a></li>
                <li class="leaf"><a href="/about/testimonials">Testimonials</a></li>
                <li class="leaf"><a href="/about/volunteers">Volunteers</a></li>
                <li class="last leaf"><a href="/about/welcome-letter">Welcome Letter</a></li>
              </ul>
            </li>
            <li class="last leaf"><a href="https://annualssc2014.eventbrite.com/" title="">Register</a></li>
          </ul>

        </section>

      </nav>
    </div>
  </div>

<!--
    <div class="large-6 medium-6 columns">
      <h1><a href="/"><img src="<?= base_path() . $directory ?>/images/logo_ssc_header_v2.png" alt="Support Staff Conference"></a></h1>
    </div>

    <div class="large-6 medium-6 scolumns">

      
      <div href="#" class="large button dropdown show-for-small">
        Navigation
        <?
          $menu = menu_navigation_links('main-menu');
          print theme('links__main-menu', array('links' => $menu));
        ?>
      </div>
      mobile nav 

      <div class="hide-for-small">

        <div class="deadline show-for-large-up">
          <p><a href="http://annualssc2013.eventbrite.com/"><span>Deadline to Register</span> April 24<sup>th</sup></a></p>
          <div class="triangle"></div>
        </div>
        <?
          $main_menu_tree = menu_tree_all_data('main-menu');
          $main_menu_expanded = menu_tree_output($main_menu_tree);
          print render($main_menu_expanded);
        ?>

      </div>

    </div>

  </div>
  <!-- @Header.end -->