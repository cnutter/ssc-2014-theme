<div class="row" >
  <div class="large-3 columns">
    <?= $fields['field_speaker_photo']->content ?>
  </div>
  <div class="large-9 columns">
    <h4><?= $fields['title']->content ?></h4>

  <? if ($speaker_title = $fields['field_speaker_title']->content) : ?>
  <h3><?= $speaker_title ?></h3>
  <? endif ?>

  <? if ($speaker_org = $fields['field_speaker_organization']->content) : ?>
    <h5><?= $speaker_org ?></h5>
  <? endif ?>


  <? if ($session = $fields['field_speaker_session']->content) : ?>
    <h6><a href="/sessions#<?= $fields['field_speaker_session_1']->content ?>"><?= $session ?></a></h6>
  <? elseif ($session = $fields['field_session_non_breakout']->content) : ?>
    <h6><?= $session ?></h6>
  <? endif ?>

  <? if ($fields['body']->content) : ?>
    <p class="bio"><strong>Biography:</strong></p>
    <?= $fields['body']->content ?>
    <span class="read_more"><?= $fields['view_node']->content ?></span>
  <? endif ?>
  </div>

</div>
