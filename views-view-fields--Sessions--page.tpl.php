<div class="row breakout-sessions">
	<div class="large-4 columns">
		<?= $fields['field_speaker_photo']->content ?>

	</div>
  <div class="large-8 columns">
    <h3><a name="<?= $fields['nid']->content ?>"><?= $fields['title']->content ?></a></h3>
	    <h4 class="hfour"><?= $fields['field_session_speaker']->content ?></h4>
	    <h5 class="hfive"><?= $fields['field_speaker_organization']->content ?></h5>
    <p class="smaller"><strong>Time:</strong> <?= $fields['field_session_starttime']->content ?><br>
    <strong>Room:</strong> <?= $fields['field_session_room']->content ?></p>
    <?= $fields['field_session_details']->content ?>
	<? if (!empty($fields['edit_node']->content)) : ?>
	    <p><?= $fields['edit_node']->content ?></p>
	<? endif ?>

  </div>
</div>
