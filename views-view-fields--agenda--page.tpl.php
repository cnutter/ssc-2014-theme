<div class="row agenda-item">
  <div class="large-4 columns">
    <p><strong><?= $fields['title']->content ?></strong></p>
  </div>
  <div class="large-8 columns">
    <?= $fields['body']->content ?>
    <? if ($fields['edit_node']->content) : ?>
      <p><?= $fields['edit_node']->content ?></p>
    <? endif ?>
  </div>
</div>
