<? require 'includes/header.inc' ?>

  <!-- @Main.start -->

    <div id="title" class="row">
      <div class="page_header_flourish">
      </div>
      <h1><?= $title ?></h1>
    </div>

    <div id="main" class="row">
          <? $tabs && print render($tabs) ?>
      <div class="large-12 columns no-pad">
        <div class="page_content">
          <?= render($title_prefix) ?> 

          <!-- @Content.start -->
          <div id="content" class="large-8 columns">
            <?= $messages ?>
            <?= render($page['content']) ?>
          </div>
          <!-- @Content.end -->

          <?= render($title_suffix) ?>


          <!-- @Sidebar.start -->
          <div id="sidebar" class="large-4 columns">
            <?php $page['sidebar_first'] && print render($page['sidebar_first']);?>
          </div>
          <!-- @Sidebar.end -->
          <div class="row clear">&nbsp;</div>
        </div>
      </div>
    </div>

  <!-- @Main.end -->

<? require 'includes/footer.inc' ?>
