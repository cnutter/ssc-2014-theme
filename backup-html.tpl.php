<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Welcome to Foundation</title>


	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

  <?php print $styles ?>

	<!-- Included CSS Files -->
  <script src="<?= base_path() . $directory ?>/javascripts/foundation/modernizr.foundation.js"></script>
</head>
<body class="<?= $classes ?>">

<?
  echo $page_top;
  echo $page;
  echo $scripts;
  echo $page_bottom;
?>

	<!-- Included JS Files (Uncompressed) -->
  <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
	<script src="<?= base_path() . $directory ?>/javascripts/foundation/jquery.foundation.buttons.js"></script>
	<script src="<?= base_path() . $directory ?>/javascripts/foundation/jquery.foundation.forms.js"></script>
	<script src="<?= base_path() . $directory ?>/javascripts/foundation/jquery.foundation.mediaQueryToggle.js"></script>
	<script src="<?= base_path() . $directory ?>/javascripts/foundation/jquery.foundation.orbit.js"></script>
	<script src="<?= base_path() . $directory ?>/javascripts/colorbox/jquery.colorbox-min.js"></script>
	<script src="<?= base_path() . $directory ?>/javascripts/jquery.lettering.js"></script>
	
  <!-- Application Javascript, safe to override -->
  <script src="<?= base_path() . $directory ?>/javascripts/foundation/app.js"></script>
</body>
</html>
