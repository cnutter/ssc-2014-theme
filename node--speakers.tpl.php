
  <div class="row one-speaker">
    <div class="large-3 columns">
<?php //print_r($node); ?>
      <? if (isset($node->field_speaker_photo['und'][0]['uri'])) :?>
        <?= theme('image_style', array('style_name' => 'headshot', 'path' => $node->field_speaker_photo['und'][0]['uri'])) ?>
      <? endif ?>
    </div>
    <div class="large-9 columns">

      <h4><?= $node->title;?></h4>

      <? if ($speaker_title = $node->field_speaker_title['und'][0]['value']) :?>
        <h3><?= $speaker_title ?></h3>
      <? endif ?>

      <? if ($speaker_org = $node->field_speaker_organization['und'][0]) :?>
        <h5>
          <?= links_to($speaker_org['title'], $speaker_org['url']) ?>
        </h5>
      <? endif ?>

      <? if ($session = $node->field_speaker_session['und'][0]['node']) : ?>
        <h6><a href="/sessions"><?= $session->title ?></a></h6>
      <? elseif ($session = $node->field_session_non_breakout['und'][0]['value']) : ?>
        <h6><?= $session ?></h6>
      <? endif ?>

      <p class="bio"><strong>Biography:</strong></p>
      <p><?= $node->body['und'][0]['value'] ?></p>
    </div>
  </div>

