<? require 'includes/header.inc' ?>

  <!-- @Slideshow.start -->
  <div class="flower">

    <div class="row slideshow">
      <div class="large-12 columns">
        <div id="slides" data-orbit>
          <?php $page['homepage_slides'] && print render($page['homepage_slides']);?>
        </div>
      </div>
    </div>

  </div>
  <!-- @Slideshow.end -->

  <!-- @Intro.start -->
  <div class="row intro">
    <div class="flourish show-for-large-up"></div>
    <div class="row">
      <div class="intro_header large-9 columns large-offset-3">
        <h2 class="kerning">Making a Difference</h2>
        <h3 class="kerning">The Annual Support Staff Conference</h3>
        <h4 class="kerning">Friday, May 2, 2014</h4>
      </div>
    </div>
    <div class="row">
      <div class="large-10 columns large-offset-2">
        <p>Our Annual Support Staff Conference (SSC) offers administrative professionals an opportunity to enjoy a day that includes networking with other administrative professionals from within our organization, school districts and County Offices. Attending this conference will help you build on your strengths, achieve goals, and refresh your career skills that are essential to any employee in a supportive role.  At our SSC discover how to sharpen your leadership edge to stay on top of your game and succeed in the workplace. Learn about motivation, attitude and find more satisfaction in your job.  Plan now to attend the best training value and professional development day ever! </p>
      </div>
    </div>
    <?php if ($user->uid) { Print '<a class="button radius" id="button-block" href="/admin/slide-order">Reorder Slides</a>';} ?>
  </div>
  <!-- @Intro.end -->
           
  <!-- @Speakers.start -->
  <div class="row speakers">
  <h2 class="section_header">2014 Speakers</h2>
    <div class="large-12 columns centered">
      <ul class="large-block-grid-5 five-up mobile-two-up">
          <? $view = views_get_view('speakers') ?>
          <? $view->set_display('block_1') ?>
          <? echo $view->preview('block_1') ?>
      </ul>
    </div>
  </div>
  <!-- @Speakers.end -->


  <!-- @Sponsors.start -->
  <div class="row sponsors">
  <h2 class="section_header">2014 Sponsors</h2>
    <div class="large-12 columns">
      <ul class="large-block-grid-3">
        <? $view = views_get_view('sponsors') ?>
        <? $view->set_display('block') ?>
        <? echo $view->preview('block') ?>
      </ul>
    </div>
  </div>
  <!-- @Sponsors.end -->

 
<? require 'includes/footer.inc' ?>
